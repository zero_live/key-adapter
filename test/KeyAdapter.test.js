const KeyAdapter = require('../src/KeyAdapter')
const { expect } = require('chai')

describe('KeyAdapter', () => {
  it('adapts a dictionary with keys in snake_case to camelCase', () => {
    const snakeDictionary = { snake_key: 'a value', another_key: 'another value' }
    const camelDictionary = { snakeKey: 'a value', anotherKey: 'another value' }

    const adaptation = new KeyAdapter(snakeDictionary).toCamelCase()

    expect(adaptation).to.deep.eq(camelDictionary)
  })

  it('adapts a dictionary with keys in camelCase to snake_case', () => {
    const snakeDictionary = { snake_key: 'a value', another_key: 'another value' }
    const camelDictionary = { snakeKey: 'a value', anotherKey: 'another value' }

    const adaptation = new KeyAdapter(camelDictionary).toSnakeCase()

    expect(adaptation).to.deep.eq(snakeDictionary)
  })
})
