const FIRST_LETTER_AFTER_UNDERSCORE = /([_][a-z])/g
const UPPER_AND_PREVIOUS_LETTER = /[\w]([A-Z])/g

module.exports = class KeyAdapter {
  constructor(dictionary) {
    this.entries = this._extractEntries(dictionary)
  }

  toCamelCase() {
    return this._adapt(this._keyToCamelCase.bind(this))
  }

  toSnakeCase() {
    return this._adapt(this._keyToSnakeCase.bind(this))
  }

  _adapt(callback) {
    const newDictionary = {}

    for (const [key, value] of this.entries) {
      const snakeKey = callback(key)

        newDictionary[snakeKey] = value
    }

    return newDictionary
  }

  _extractEntries(dictionary) {
    return Object.entries(dictionary)
  }

  _keyToCamelCase(string) {
    return string.replace(FIRST_LETTER_AFTER_UNDERSCORE, this._toUpperCase)
  }

  _keyToSnakeCase(string) {
    return string.replace(UPPER_AND_PREVIOUS_LETTER, this._brokeUpWithUnderscore)
  }

  _toUpperCase(letter) {
    const withoutUnderscore = letter.replace('_', '')

    return withoutUnderscore.toUpperCase()
  }

  _brokeUpWithUnderscore(letters) {
    const lastLetterOfPreviousWord =  letters[0]
    const firstLetterOfCurrentWord = letters[1]
    const broken = lastLetterOfPreviousWord + '_' + firstLetterOfCurrentWord

    return broken.toLowerCase()
  }
}
